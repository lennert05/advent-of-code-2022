use axum::Json;
use serde_json::{json, Value};

#[derive()]
enum Overlap {
    NoOverlap,
    Overlap,
    Contained,
}

pub async fn cleanup(body: String) -> Json<Value> {
    let input: Vec<_> = body.lines().map(|line| line.split_once(',')).collect();

    let process: Vec<Overlap> = input
        .iter()
        .map(|pair| {
            let p = pair.unwrap();
            let a = to_tuple(p.0);
            let b = to_tuple(p.1);
            is_overlapping(a, b)
        })
        .collect();

    let mut overlap: i32 = 0;
    let mut contained: i32 = 0;
    let mut no_overlap: i32 = 0;

    for item in process {
        match item {
            Overlap::Overlap => overlap += 1,
            Overlap::Contained => contained += 1,
            Overlap::NoOverlap => no_overlap += 1,
        }
    }

    let result = json!({"overlap": overlap, "contained": contained, "no_overlap": no_overlap});

    Json(json!(result))
}

fn to_tuple(s: &str) -> (i32, i32) {
    match s
        .split("-")
        .filter_map(|p| p.trim().parse().ok())
        .take(2)
        .collect::<Vec<_>>()[..]
    {
        [a, b] if b >= a => (a, b),
        [a, b] if a > b => (b, a),
        _ => (0, 0),
    }
}

fn is_overlapping(a: (i32, i32), b: (i32, i32)) -> Overlap {
    if a.1 < b.0 || a.0 > b.1 {
        return Overlap::NoOverlap;
    }
    if a.0 >= b.0 && a.1 <= b.1 {
        return Overlap::Contained;
    }
    if b.0 >= a.0 && b.1 <= a.1 {
        return Overlap::Contained;
    }
    Overlap::Overlap
}
