use axum::Json;

use serde_json::{json, Value};

pub async fn calculate_most_efficient_elf(body: String) -> Json<Value> {
    let input: Vec<&str> = body.split("\n\n").collect();

    let vec_of_calories: Vec<Vec<i32>> = input
        .iter()
        .map(|e| e.split('\n').map(|n| n.parse().unwrap_or(0)).collect())
        .collect();
    let mut vec_of_elf: Vec<i32> = vec_of_calories.iter().map(|a| a.iter().sum()).collect();
    vec_of_elf.sort();
    vec_of_elf.reverse();

    Json(json!(vec_of_elf[0]))
}
