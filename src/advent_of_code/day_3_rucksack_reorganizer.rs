use axum::Json;
use serde_json::{json, Value};

pub async fn rucksack_reorganizer(body: String) -> Json<Value> {
    let input: Vec<_> = body
        .lines()
        .map(|line| line.split_at(line.len() / 2))
        .collect();

    let item_per_compartment: Vec<Option<char>> = input
        .iter()
        .map(|pair| pair.0.chars().find(|character| pair.1.contains(*character)))
        .collect();

    let line_prio: Vec<i32> = item_per_compartment
        .iter()
        .map(|value| value.unwrap_or('0'))
        .map(|prio| match prio {
            'A'..='Z' => (prio as u8 - 65 + 27) as i32,
            'a'..='z' => (prio as u8 - 96) as i32,
            _ => 0,
        })
        .collect();
    let result: i32 = line_prio.iter().sum();
    Json(json!(result))
}
