use axum::Json;
use serde_json::{json, Value};

pub async fn rock_paper_scissors(body: String) -> Json<Value> {
    let input: Vec<i32> = body
        .split("\n")
        .map(|strategy| match_grade(strategy))
        .collect();
    let result: i32 = input.iter().sum();
    Json(json!(result))
}

fn match_grade(strategy: &str) -> i32 {
    match strategy {
        "A X" => 1 + 3,
        "A Y" => 2 + 6,
        "A Z" => 3 + 0,
        "B X" => 1 + 0,
        "B Y" => 2 + 3,
        "B Z" => 3 + 6,
        "C X" => 1 + 6,
        "C Y" => 2 + 0,
        "C Z" => 3 + 3,
        _ => 0,
    }
}
