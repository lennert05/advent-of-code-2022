mod advent_of_code;
mod router;

use router::router::router as site_router;

#[tokio::main]
async fn main() {
    // build our application with a single route
    let app = site_router();

    // run it with hyper on localhost:3000
    axum::Server::bind(&"0.0.0.0:3000".parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap();
}
