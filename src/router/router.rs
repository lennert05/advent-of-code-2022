use axum::{routing::post, Router};

use crate::advent_of_code::day_1_elf_calories_carrying::calculate_most_efficient_elf;
use crate::advent_of_code::day_2_rock_paper_scissors::rock_paper_scissors;
use crate::advent_of_code::day_3_rucksack_reorganizer::rucksack_reorganizer;
use crate::advent_of_code::day_4_camp_cleanup::cleanup;

pub fn router() -> Router {
    Router::new()
        .route("/day/1", post(calculate_most_efficient_elf))
        .route("/day/2", post(rock_paper_scissors))
        .route("/day/3", post(rucksack_reorganizer))
        .route("/day/4", post(cleanup))
}
